var $ = jQuery;

$(document).ready(function() {
    $('.fl-module-photo').each(function() {
        var getFLPhotoURL = $(this).find('.fl-photo:eq(0)').children('.fl-photo-content').children('a').attr('href');
        var getFLPhotoTarget = $(this).find('.fl-photo:eq(0)').children('.fl-photo-content').children('a').attr('target');
        if (getFLPhotoURL) {
            $(this).find('.fl-photo:eq(0)').children('.fl-photo-caption').wrapInner('<a href="' + getFLPhotoURL + '" target="' + getFLPhotoTarget + '" />');
        }
    });
    $(document).ready(function(){
        $('#philadelphia-flooring-wrap a').attr('target', '_blank');
      });
});


